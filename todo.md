# Todo

- [x] For nouns, only add the meadning of the noun. not the adjective section
- [ ] Integrate [Pons API](https://sl.pons.com/p/spletni-slovar/developers/api)
- [ ] Organize code in a linguee library, which is just called an returns json(?) or a Translation() object? 
    could be a dict? 
- [ ] use try-catch blocks


Sample output of https://www.npmjs.com/package/linguee/v/1.0.1

Call in npm js
```js
var linguee = require('linguee');
 
// translate money from english to portuguese
 
linguee
  .translate('money', { from: 'eng', to: 'por' })
  .then(function(response) {
    console.log(response);
  })
  .catch(function(error) {
    // ...
  });
```

Return json object
```json
{
"query": "money",
"words": [
  {
    "term": "money",
    "audio": "https://www.linguee.com/mp3/EN_US/97/9726255eec083aa56dc0449a21b33190-101",
    "additionalInfo": null,
    "type": "noun",
    "translations": [
      {
        "term": "dinheiro",
        "audio": "https://www.linguee.com/mp3/PT_BR/79/79de84b440a2419610355782ac900622-106",
        "type": "noun, masculine",
        "alternatives": [],
        "examples": [
          {
            "from": "My friend still owes me some money. ",
            "to": "Meu amigo ainda me deve algum dinheiro."
          },
          {
            "from": "I want to withdraw money from my account.",
            "to": "Eu quero retirar dinheiro da minha conta."
          }
        ]
      }
    ],
    "lessCommonTranslations": [
      {
        "term": "capital",
        "type": "noun, masculine",
        "usage": null
      },
      {
        "term": "moeda",
        "type": "noun, feminine",
        "usage": null
      },
      {
        "term": "verba",
        "type": "noun, feminine",
        "usage": null
      },
      {
        "term": "riqueza",
        "type": "noun, feminine",
        "usage": null
      },
      {
        "term": "numerário",
        "type": "noun, masculine",
        "usage": null
      },
      {
        "term": "fortuna",
        "type": "noun, feminine",
        "usage": null
      }
    ]
  },
  {
    "term": "money",
    "audio": "https://www.linguee.com/mp3/EN_UK/97/9726255eec083aa56dc0449a21b33190-0",
    "additionalInfo": null,
    "type": "noun as adjective",
    "translations": [],
    "lessCommonTranslations": [
      {
        "term": "monetário",
        "type": "adjective, masculine",
        "usage": null
      },
      {
        "term": "financeira",
        "type": "adjective, feminine",
        "usage": null
      },
      {
        "term": "monetária",
        "type": "adjective, feminine",
        "usage": null
      },
      {
        "term": "cambial",
        "type": "adjective, singular, both",
        "usage": null
      }
    ]
  }
],
"examples": [
  {
    "from": {
      "content": "value for money",
      "type": "noun",
      "audio": "https://www.linguee.com/mp3/EN_UK/78/78d0975a374ecdf77205be9b213ee294-0"
    },
    "to": [
      {
        "content": "relação qualidade-preço",
        "type": "noun, feminine"
      },
      {
        "content": "rentabilidade",
        "type": "noun, feminine"
      }
    ]
  },
  {
    "from": {
      "content": "raise money",
      "type": "verb",
      "audio": "https://www.linguee.com/mp3/EN_US/55/55aace5542808e206cd7c48c63fa386a-200"
    },
    "to": [
      {
        "content": "levantar dinheiro",
        "type": "verb"
      }
    ]
  },
  {
    "from": {
      "content": "money supply",
      "type": "noun",
      "audio": "https://www.linguee.com/mp3/EN_US/72/72d6c1f1aa121245beb37ff5b58ae4c7-101"
    },
    "to": [
      {
        "content": "massa monetária",
        "type": "noun, feminine"
      },
      {
        "content": "fornecimento de dinheiro",
        "type": "noun, masculine"
      }
    ]
  }
]
}
```

