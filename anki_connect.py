import json
import urllib.request


def build_json(action, **params):
    return {"action": action, "params": params, "version": 6}


def invoke(action, **params):
    requestJson = json.dumps(build_json(action, **params)).encode("utf-8")
    print(requestJson)

    response = json.load(
        urllib.request.urlopen(
            urllib.request.Request("http://localhost:8765", requestJson)
        )
    )
    if len(response) != 2:
        raise Exception("response has an unexpected number of fields")
    if "error" not in response:
        raise Exception("response is missing required error field")
    if "result" not in response:
        raise Exception("response is missing required result field")
    if response["error"] is not None:
        raise Exception(response["error"])
    return response["result"]


# invoke("createDeck", deck="test1")
print(invoke("version"))
print(f'decks: {invoke("deckNames")}')
print(f'model names: {invoke("modelNames")}')


def create_card(query: str, result: str):
    request = {
        "action": "addNote",
        "version": 6,
        "params": {
            "note": {
                "deckName": "Englisch",
                "modelName": "Basic",
                "fields": {"Front": f"{query}", "Back": f"{result}"},
                "options": {"allowDuplicate": false},
                "tags": [],
            }
        },
    }

    request_json = json.dumps(request)
    print(request_json)




# {
#     "action": "addNote",
#     "version": 6,
#     "params": {
#         "note": {
#             "deckName": "Default",
#             "modelName": "Basic",
#             "fields": {
#                 "Front": "front content",
#                 "Back": "back content"
#             },
#             "options": {
#                 "allowDuplicate": false
#             },
#             "tags": [
#                 "yomichan"
#             ],
#             "audio": [{
#                 "url": "https://assets.languagepod101.com/dictionary/japanese/audiomp3.php?kanji=猫&kana=ねこ",
#                 "filename": "yomichan_ねこ_猫.mp3",
#                 "skipHash": "7e2c2f954ef6051373ba916f000168dc",
#                 "fields": [
#                     "Front"
#                 ]
#             }]
#         }
#     }
# }
