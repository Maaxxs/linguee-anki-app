# ReadMe

In general, this is a very simple script and a lot of values are hardcoded for my specific use.

## Preconditions

- You need a Deck in Anki called `Englisch`. If you want to add the notes to a different deck, change the name of the deck in the code.
- The URL has as `source` parameter (source language) `en`. If you want to use `auto` or another language, change it.

## Usage

- You need the [Anki Connect Addon](https://ankiweb.net/shared/info/2055492159) installed
- Anki needs to be running while you execute this script (the Anki Connect Addon is listening on `localhost:8765`)

Execute with

```sh
python3 main.py precious
python3 main.py "precious time"
```

You can make it executable with `chmod 700 main.py` and run it like so

```sh
./main.py precious
python3 main.py "precious time"
```

## Linguee

This script is only for personal use.

Read their [Terms and Conditions](https://www.linguee.com/english-russian/page/termsAndConditions.php) and [Privacy statement](https://www.linguee.com/english-russian/page/privacy.php).
