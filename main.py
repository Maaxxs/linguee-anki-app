#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
import json
from sys import argv

API_URL = "http://localhost:8765"


def query_linguee(query: str):
    """ Query linguee with the given english word.
    Return a tuple. The first element is the query 
    (which can be changed inside this function) and the 
    second element is the result div container"""

    print(f"Suche: {query}\n")
    url = f"https://www.linguee.de/deutsch-englisch/search?source=en&query={query}"
    response = requests.get(url)

    soup = BeautifulSoup(response.text, features="html.parser")

    # check if we had a typo in the query, but linguee was smart enough
    # to display the right word anyway
    didyoumean = soup.find("h1", attrs={"class": "didyoumean"})
    if didyoumean:
        similiar = didyoumean.find("span", attrs={"class": "corrected"}).text
        yes = input(f'Meintest du "{similiar}"? [Y/n]').lower()
        if yes == "" or yes == "y":
            return query_linguee(similiar)
        else:
            exit(0)

    # find the exact meanings of the query
    # only find the first occurence of <div class="lemma featured">
    # is None if it didn't find the PageElement
    div_exact = soup.find("div", attrs={"class": "lemma featured"})

    if div_exact:
        return query, div_exact

    # if we did not find a "did you mean" and a "lemma featured"
    # then we did not find a result for that query and the class
    # "noresults" should be present
    result = soup.find("h1", attrs={"class": "noresults"})
    if result:
        print(f"Kein Ergebnis für {query} gefunden.")
        exit(0)
    else:
        print(
            'Could not find the div with class="lemma featured" \
            and could not find the class="noresults".'
        )
        print("This could mean that linguee changed their DOM.")
        print(
            "As well, it might be that linguee didn't find a result for our query, \
            but displayed some other information and therefore the class \
            'noresult' is not present. The DOM might still be the same."
        )
        exit(0)


def read_html():
    """ Read html DOM from file.
    Only used for testing.
    """
    with open("precious.html", "r") as file:
        content = file.read()

    soup = BeautifulSoup(content, features="html.parser")

    div_exact = soup.find_all("div", attrs={"class": "exact"})
    div_inexact = soup.findAll("div", attrs={"class": "inexact"})

    try:
        div_exact = BeautifulSoup(str(div_exact[0]), features="html.parser")
    except IndexError:
        div_exact = None

    try:
        div_inexact = BeautifulSoup(str(div_inexact[0]), features="html.parser")
    except IndexError:
        div_inexact = None

    return (div_exact, div_inexact)


def get_exact_meaning(div: BeautifulSoup):
    """ Return the exact featured meanings. """
    # class = "dictLink featured"

    # spans = div.find_all("span", attrs={"class": "tag_trans"})
    # for span in spans:
    #     # in the tag there is a non-breaking space \xa0 in Latin1, which we replace with a space.
    #     tag_type = span.find_next('span', attrs={"class": "tag_type"}).get('title').replace('\xa0', ' ')
    #     meaning = span.find_next("a", attrs={"class": "dictLink featured"}).text
    #     print(f'Type: {tag_type}, meaning: {meaning}')
    #     word_type.setdefault(tag_type, [])
    #     word_type[tag_type].append(meaning)

    # key: type, value: list of meanings for that type
    word_type = dict()

    links = div.find_all("a", attrs={"class": "dictLink featured"})
    for link in links:
        tag_type = (
            link.find_next("span", attrs={"class": "tag_type"})
            .get("title")
            .split(",")[0]
        )

        word_type.setdefault(tag_type, [])
        word_type[tag_type].append(link.text)

        # print(f"{link.text}, {tag_type}")

    for key, values in word_type.items():
        print(f"{key}:")
        for value in values:
            print(f"  {value}")

    # Return a html construct. The key (type like adjective, adverb, substantiv)
    # has a smaller font size
    result = ""
    for key, values in word_type.items():
        result += '<br><div style="font-size: 14px;">' + key + "</div><br><br>"
        for value in values:
            result += value + "<br>"

    return result


def create_card(query: str, result: str):
    """Create a note in Anki. Only works if the anki
    addon "Anki connect" is installed and anki connect 
    is listening on localhost:8765 (default)"""

    request = {
        "action": "addNote",
        "version": 6,
        "params": {
            "note": {
                "deckName": "Englisch",
                "modelName": "Basic",
                "fields": {"Front": f"{query}", "Back": f"{result}"},
                "options": {"allowDuplicate": False},
                "tags": [],
            }
        },
    }

    response = json.loads(requests.request("POST", API_URL, json=request).text)

    if response["error"] == None:
        print("Successfully added card to Anki.")
    else:
        print(response["error"])


def check_connection():
    """Check if Anki is running and if it is listening on port
    8765 with the Anki Connect Plugin
    """

    request = {"action": "version", "version": 6}

    try:
        requests.request("POST", API_URL, json=request)
    except Exception as e:
        print("Error: Cannot connect to Anki.")
        print("Is Anki running and did you install this Anki Connect Addon?")
        print("Anki Connect Addon: https://ankiweb.net/shared/info/2055492159")
        # print(e)
        # print(e.__class__.__name__)
        exit(1)


if __name__ == "__main__":

    if len(argv) != 2:
        print(f'Usage: {argv[0]} "query word"')
        print("Example:")
        print(f"python3 {argv[0]} precious")
        print(f'python3 {argv[0]} "precious time"')
        exit(0)

    query = argv[1]

    query, div_exact = query_linguee(query)

    meaning = get_exact_meaning(div_exact)

    if not meaning:
        # This is usually not the case
        print("Result is empty")
        exit(0)

    check = input("\nDo you want to add a card to Anki? [Y/n]").lower()

    if check == "" or check == "y":
        check_connection()
        create_card(query, meaning)
